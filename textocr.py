# import the necessary packages
from PIL import Image
import pytesseract
import argparse
import cv2
import os
import numpy as np
import re

from tensorflow.python.keras._impl.keras.preprocessing.image import load_img, img_to_array

process = "thresh"


# image = cv2.imread("cartes-test/carte-mali/8542167.jpg")
# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# gray = cv2.resize(gray,(900,600))'


def performOcr(imageFile, pays):
    image = load_img(imageFile)
    image = img_to_array(image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.resize(gray, (900, 600))

    h = gray.shape[0]
    w = gray.shape[1]
    for y in range(0, h):
        for x in range(0, w):
            if(pays == "senegal"):
                if ((x < 0.30 * w) | ((x> 0.90*w) & (y<0.87*h)) | (y <0.22*h)):
                    T = 0
                else:
                    T = 85

            elif ((pays == "mali")):
                if ((x < 0.27 * w) | ((x> 0.80*w) & (y<0.87*h)) | (y <0.15*h)):
                    T = 0


                elif ((x > 0.75 * w)):
                    if ((y < 0.85 * h)):
                        T = 100

                    elif (y < 0.20 * h):
                        T = 0

                else:
                    if(y < 0.4*h):
                        T = 100
                    else:
                        T = 85

            if (pays == "guinee"):
                if ((x < 0.38 * w) ):
                    T = 0
                elif ((y > 0.35 * h) & (y < 0.40 * h)):
                    T = 130
                elif ((y < 0.22 * h) | (y > 0.6 * h)):
                    T = 0
                else:
                    T = 125
            elif (pays == "mauritanie"):
                if ((x < 0.27 * w) | ((x> 0.80*w) & (y<0.87*h)) | (y <0.15*h)):
                    T = 0
                elif ((y < 0.24 * h) | (y > 0.86 * h) | ((y > 0.60 * h) & (x > 0.57 * w))):
                    T = 0
                elif ((y > 0.24 * h) & (y < 0.30 * h)):
                    T = 130

                elif ((y > 0.30 * h) & (y < 0.4 * h)):
                    T = 142
                else:
                    T = 130

            gray[y, x] = 255 if gray[y, x] >= T else 0

    if process == "thresh":
        blur = cv2.GaussianBlur(gray, (5, 5), -5)
        blur = blur.astype(np.uint8)
        ret3, th3 = cv2.threshold(blur, 255, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        gray = cv2.adaptiveThreshold(th3, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                     cv2.THRESH_BINARY, 11, 10)
        # gray = cv2.medianBlur(gray, 3)

    # make a check to see if median blurring should be done to remove
    # noise

    elif process == "blur":
        gray = cv2.medianBlur(gray, 3)

    # grab the (x, y) coordinates of all pixel values that
    # are greater than zero, then use these coordinates to
    # compute a rotated bounding box that contains all
    # coordinates
    # coords = np.column_stack(np.where(gray > 0))
    # angle = cv2.minAreaRect(coords)[-1]
    #
    # # the `cv2.minAreaRect` function returns values in the
    # # range [-90, 0); as the rectangle rotates clockwise the
    # # returned angle trends to 0 -- in this special case we
    # # need to add 90 degrees to the angle
    # if angle < -45:
    #     print(angle)
    #     angle = -(90 + angle)
    #
    #
    # # otherwise, just take the inverse of the angle to make
    # # it positive
    # else:
    #     angle = -angle
    #
    # # rotate the image to deskew it
    # (h, w) = gray.shape[:2]
    # center = (w // 2, h // 2)
    # M = cv2.getRotationMatrix2D(center, angle, 1.0)
    # rotated = cv2.warpAffine(gray, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
    #
    # # draw the correction angle on the image so we can validate it
    # cv2.putText(rotated, "Angle: {:.2f} degrees".format(angle), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
    #
    # # show the output image
    # print("[INFO] angle: {:.3f}".format(angle))
    # cv2.imshow("Input", gray)
    # # cv2.imshow("Rotated", rotated)
    cv2.waitKey(0)
    filename = "test.jpg".format(os.getpid())
    cv2.imwrite(filename, gray)
    text = pytesseract.image_to_string(Image.fromarray(gray), lang="eng")

    print(text)
    # if (pays == "mali"):
    #     num = text.splitlines()[0]
    #     nom = text.splitlines()[1]
    #     print(num,nom)
    donnees = []
    l = None
    for line in text.splitlines():
        if (l == 1):
            l = 2
        if (pays == "mali"):

            m1 = re.search(r"(^(\s*(\d+[^/]\s*){2,}[A-Za-z]*)+)", line)
            if (m1 != None):
                donnees.append(m1.group(0))
            m = re.search(r"^((\s*[A-Z]+(\s*[a-z]*)*)+)", line)
            m3 = re.search(r"^((\s*\d{2}/\d{2}/\d{4}))", line)
            if (m3 != None):
                donnees.append(m3.group(2))
            if ((m != None)):
                donnees.append(m.group(0))
        elif (pays == "guinee"):

            m = re.search(r"(^\s*N{1}.{1}\s*(\d+))", line)
            m2 = re.search(r"^(Nom (([A-Za-z]\s*)+))", line)
            m3 = re.search(r"(Prénom.? (([A-Za-z]\s*)+))", line)
            m4 = re.search(r"(Sexe.? (([A-Za-z]\s*)+))", line)
            m5 = re.search(r"(N.* (\d{2}/\d{2}/\d{4}))", line)
            if ((m != None)):
                donnees.append(m.group(2))
            if ((m2 != None)):
                donnees.append(m2.group(2))
            if ((m3 != None)):
                donnees.append(m3.group(2))
            if ((m4 != None)):
                donnees.append(m4.group(2))
            if ((m5 != None)):
                donnees.append(m5.group(2))
                l = 1
            if (l == 2):
                donnees.append(line)
                l = 0
        elif (pays == "mauritanie"):
            m = re.search(r"(^\s*(\d+))", line)
            m2 = re.search(r"^((\s*[A-Z]+(\s*[a-z]*)*)+)", line)
            m3 = re.search(r"^((\s*\d{2}\s*([A-Za-z]+\s*)+/([A-Za-z]\s*)+\d{4}))", line)
            m4 = re.search(r"^\s*$", line)

            if ((m != None)):
                donnees.append(m.group(2))
                l = 0
            if ((l != None) & (m4 == None)):

                l = l + 1
                if ((l % 2 == 0) & ((m2 != None) & (m3 == None))):
                    donnees.append(line)
            if (m3 != None):
                donnees.append(m3.group(2))

    # print(donnees)

    #     print("+++++++++----------++++++++++")

    # show the output images
    # cv2.imshow("Image", gray)
    # cv2.imshow("Output", gray)
    # cv2.waitKey(0)
    return donnees


def orderData(donnees, pays):
    infos = None
    if (pays == 'mali'):
        infos = {
            'numero': donnees[0],
            'prenom': donnees[1],
            'nom': donnees[2],
            'dateDeNaissance': donnees[3],
            'lieuDeNaissance': donnees[4],
            'NomDeLaMere': donnees[5],
            'NomDuPere': donnees[6],
            'Profession': donnees[6]
        }
    elif (pays == 'guinee'):
        infos = {
            'numero': donnees[0],
            'nom': donnees[1],
            'prenom': donnees[2],
            'sexe': donnees[3],
            'dateDeNaissance': donnees[4],
            'lieuDeNaissance': donnees[5],
            # 'NomDuPere': donnees[6],
            # 'NomDeLaMere': donnees[7],
            # 'Profession': donnees[8]
        }
    elif(pays == "mauritanie"):

        infos = {

            'numero': donnees[0],
            'prenom': donnees[1],
           'NomDuPere': donnees[2],
           'nom': donnees[3],
           'sexe': donnees[4],
             'dateDeNaissance': donnees[6],
            #'lieuDeNaissance': donnees[7],
        }
    elif(pays == "senegal"):
        infos = {

            'numero': donnees[0],
            'prenom': donnees[1],
            'NomDuPere': donnees[2],
            'nom': donnees[3],
            'sexe': donnees[4],
            'dateDeNaissance': donnees[6],
            # 'lieuDeNaissance': donnees[7],
        }

    return infos


performOcr("ocr_senegal.jpg", "senegal")
