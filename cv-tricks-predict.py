from tensorflow.python.keras import Model
from tensorflow.python.keras._impl.keras.applications import VGG16, InceptionV3
from tensorflow.python.keras._impl.keras.layers import GlobalAveragePooling2D, Dense, Dropout, Flatten
from tensorflow.python.keras._impl.keras.preprocessing.image import load_img, img_to_array
import numpy as np
img_size=224
# base_model = VGG16(include_top=False, weights=None, input_shape = (img_size, img_size, 3))
base_model = InceptionV3(weights=None, include_top=False, input_shape=(img_size, img_size, 3))
x = base_model.output
x = GlobalAveragePooling2D()(x)
x = Flatten()(x)
x = Dense(1024, activation="relu")(x)
x = Dropout(0.5)(x)
x = Dense(128, activation='sigmoid')(x)
classes = 5
imageFile= "carte-validation/carte-mauritanie/8054887.jpg"
predictions = Dense(classes, activation='softmax')(x)

model = Model(inputs=base_model.input, outputs=predictions)

model.load_weights("cv-tricks_fine_tuned_model5.h5")

inputShape = (224,224) # Assumes 3 channel image
image = load_img(imageFile, target_size=inputShape)
image = img_to_array(image)   # shape is (224,224,3)
image = np.expand_dims(image, axis=0)  # Now shape is (1,224,224,3)!

image = image/255.0

preds = model.predict(image)
print(preds)