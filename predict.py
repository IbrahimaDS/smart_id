import tensorflow as tf
import numpy as np
import os,glob,cv2
import sys,argparse
import operator
import random

import tensorflow as tf

import numpy as np
import matplotlib.pyplot as plt

# First, pass the path of the image
from dataloader import DataLoader
from dataset import load_train

filename = 'cartes-test/carte-mauritanie/8520504.jpg'
image_size=128
num_channels=3
images1, labels1,img_names, cls  = load_train("./cartes-test", 128,["carte-guinee","carte-mali","carte-mauritanie"])
print(images1)
images = images1
# Reading the image using OpenCV
# image = cv2.imread(filename)
# Resizing the image to our desired size and preprocessing will be done exactly as done during training
# image = cv2.resize(image, (image_size, image_size),0,0, cv2.INTER_LINEAR)
# images.append(image)
images = np.array(images, dtype=np.uint8)
images = images.astype('float32')
images = np.multiply(images, 1.0/255.0)
#The input to the network is of shape [None image_size image_size num_channels]. Hence we reshape.
# x_batch = images.reshape(1, image_size,image_size,num_channels)

## Let us restore the saved model
sess = tf.Session()
# Step-1: Recreate the network graph. At this step only graph is created.
saver = tf.train.import_meta_graph('./modeldeeplearning.meta')
# Step-2: Now let's load the weights saved using the restore method.
saver.restore(sess, tf.train.latest_checkpoint('./'))

# Accessing the default graph which we have restored
graph = tf.get_default_graph()

# Now, let's get hold of the op that we can be processed to get the carte-mauritanie.
# In the original network y_pred is the tensor that is the prediction of the network
y_pred = graph.get_tensor_by_name("y_pred:0")

## Let's feed the images to the input placeholders
x= graph.get_tensor_by_name("x:0")
y_true = graph.get_tensor_by_name("y_true:0")
y_test_images = np.zeros((1, 2))


### Creating the feed_dict that is required to be fed to calculate y_pred
# feed_dict_testing = {x: x_batch, y_true: y_test_images}

sample_indexes = random.sample(range(len(images)), 10)
sample_images = [images1 [i] for i in sample_indexes]
sample_labels = [labels1[i] for i in sample_indexes]
predicted = sess.run(y_pred, feed_dict={
    x: images
})
#result=sess.run(y_pred, feed_dict={feed_dict_testing})
# result is of this format [probabiliy_of_rose probability_of_sunflower]
print(predicted)
fig = plt.figure(figsize=(15, 15))
for i in range(len(sample_images)):
    truth = sample_labels[i]
    print(truth)
    prediction = predicted[i]

    trueIndex, _ = max (enumerate(truth), key=operator.itemgetter(1))
    index, value = max(enumerate(prediction), key=operator.itemgetter(1))
    print(index,value)

    plt.subplot(5, 3, 1 + i)
    plt.axis('off')
    if (index == trueIndex):
        color = 'green'
    else:
        color = 'red'

    plt.text(40, 10, "Truth:        {0}\nPrediction: {1}    \nProbabilité: {2}".format(prediction[index],prediction[index] ,prediction[index]),
             fontsize=12, color=color)
    plt.imshow(sample_images[i], cmap="gray")

plt.show()