import random

import tensorflow as tf

import numpy as np
import matplotlib.pyplot as plt

from dataloader import DataLoader
import operator

from dataset import load_train


def getpays(index):
    payspredicted = "mauritanie" if (index == 2 ) else  "mali" if (index== 1) else "guinnee"

    return payspredicted

## Let us restore the saved model
sess = tf.Session()
# Step-1: Recreate the network graph. At this step only graph is created.
saver = tf.train.import_meta_graph('./modeldeeplearning.meta')
# Step-2: Now let's load the weights saved using the restore method.
saver.restore(sess, tf.train.latest_checkpoint('./'))

# Accessing the default graph which we have restored
graph = tf.get_default_graph()

# Now, let's get hold of the op that we can be processed to get the carte-mauritanie.
# In the original network y_pred is the tensor that is the prediction of the network
softmax = graph.get_tensor_by_name("y_pred:0")

# [print(n.name) for n in tf.get_default_graph().as_graph_def().node]

## Let's feed the images to the input placeholders
x= graph.get_tensor_by_name("x:0")
y_true = graph.get_tensor_by_name("y_true:0")


print(x)


images1, labels1,img_names, cls = load_train("./cartes-test/", 128,["guinee","mali","mauritanie"])

# np.savetxt("data.txt", images1, delimiter=",")
sample_indexes = random.sample(range(len(images1)), 1)
sample_images = [images1[i] for i in sample_indexes]
sample_labels = [labels1[i] for i in sample_indexes]
feed_dict_testing = {x: sample_images, y_true: sample_labels}

# Run the "correct_pred" operation
predicted = sess.run(softmax, feed_dict={
    x: sample_images
})

# Print the real and predicted labels
print(sample_labels)
print(predicted)

# Display the predictions and the ground truth visually.
fig = plt.figure(figsize=(15, 15))
for i in range(len(sample_images)):
    truth = sample_labels[i]
    print(truth)
    prediction = predicted[i]

    trueIndex, _ = max (enumerate(truth), key=operator.itemgetter(1))
    index, value = max(enumerate(prediction), key=operator.itemgetter(1))
    print(index,value)

    plt.subplot(5, 3, 1 + i)
    plt.axis('off')
    if (index == trueIndex):
        color = 'green'
    else:
        color = 'red'
    payspredicted = getpays(index)
    plt.text(40, 10, "Truth:        {0}\nPrediction: {1}    \nProbabilité: {2}".format(getpays(trueIndex),payspredicted ,prediction[index]),
             fontsize=12, color=color)
    plt.imshow(sample_images[i], cmap="gray")

plt.show()







## Creating the feed_dict that is required to be fed to calculate y_pred

# result=sess.run(y_pred, feed_dict=feed_dict_testing)
# result is of this format [probabiliy_of_rose probability_of_sunflower]
# print(result)
