import os

import cv2


import numpy as np
from numpy import array

from FindContour import FindCountourAndResize


class DataLoader:
    def __init__(self, data_directory):
        self.data_directory = data_directory

    def load_data(self):
        directories = [d for d in os.listdir(self.data_directory)
                       if os.path.isdir(os.path.join(self.data_directory, d))]
        labels = []
        images = []
        # self.preprocess();
        for d in directories:
            print(d)
            label_directory = os.path.join(self.data_directory, d)
            file_names = [os.path.join(label_directory, f)
                          for f in os.listdir(label_directory)

                          if (f.endswith(".jpg") | f.endswith(".jpeg"))]
            for f in file_names:
                images.append(cv2.resize(cv2.imread(f), (300, 300)))
                if (d == "carte-guinee"):
                    labels.append([1., 0., 0.])
                elif (d == "carte-mali"):
                    labels.append([0., 1., 0.])
                elif (d == "carte-mauritanie"):
                    labels.append([0., 0., 1.])

                elif (d == "autres"):
                    labels.append([0., 0., 0.])
        labels = np.array(labels)
        images = np.array(images)

        # labels[0].append[labelmali]
        # print(labels)
        # images2 = [cv2.resize(image, (300, 300)) for image in images]

        return images, labels

    def preprocess(self,label_directory = "./cnicedeao"):

        file_names = [os.path.join(label_directory, f)
                      for f in os.listdir(label_directory)

                      if f.endswith(".jpg")]


        for f in file_names:
            FindCountourAndResize(f).findCountour()
        # directory = "./cnicedeao/"
        # print(self.data_directory)
        # directories = [d for d in os.listdir(directory)
        #                if os.path.isdir(os.path.join(directory, d))]
        #
        # labels = []
        # images = []
        #
        # for d in directory:
        #
        #     label_directory = os.path.join(directory, d)
        #     file_names = [os.path.join(label_directory, f)
        #                   for f in os.listdir(label_directory)
        #
        #                   if f.endswith(".jpg")]
        #     for f in file_names:
        #         FindCountourAndResize(f).findCountour()
        #         print(f)

