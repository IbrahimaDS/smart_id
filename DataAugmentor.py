import Augmentor

p = Augmentor.Pipeline("C:\\Users\\ibrahima\\Documents\\carte-cdeao\\cni-senegal")
p.rotate(probability=0.8, max_left_rotation=15, max_right_rotation=15)
p.zoom(probability=0.2, min_factor=0.8, max_factor=1.2)
p.sample(300)

p.process()
