import tensorflow as tf
import numpy as np
import cv2

from dataloader import DataLoader
from utilities import random_mini_batches

"""Model function for CNN."""
beta = 0.01
image_size = 300
num_labels = 3

NUM_PIXELS = image_size*image_size
NUM_CLASSES = 3
learning_rate = 0.001
logs_path = "tmp/carte-mauritanie/4"
num_channel  = 3
minibatch_size = 100


def add_dense_hidden_layer_op(x, num_neurons_previous_layer, num_neurons_current_layer, name_scope):
    with tf.name_scope(name_scope):
        # Model parameters
        weights = tf.Variable(
            tf.truncated_normal(shape=[num_neurons_previous_layer, num_neurons_current_layer], stddev=0.1,
                                name="weights"))
        biases = tf.Variable(tf.constant(0.1, shape=[num_neurons_current_layer], name="biases"))

    # Activation function


    relu = tf.nn.relu(tf.matmul(x, weights) + biases, name=name_scope)

# TensorBoard Summary
    tf.summary.histogram(relu.op.name + '/activations', relu)

    return relu


def add_softmax_op(x, num_neurons_previous_layer, num_classes, name_scope="softmax"):
    with tf.name_scope(name_scope):
        # Model parameters
        weights = tf.Variable(tf.zeros([num_neurons_previous_layer, num_classes]))
        biases = tf.Variable(tf.zeros([num_classes]))

        # Logits and softmax layer
        logits = tf.matmul(x, weights) + biases
        softmax = tf.nn.softmax(logits)

        # TensorBoard Summary
        tf.summary.histogram(softmax.op.name + '/activations', x)

        return softmax, logits,weights


def add_conv_op(x, num_pixels_conv, num_channels_in_previous_layer, num_channels_in_current_layer, name_scope,strid):
    with tf.name_scope(name_scope):
        # Model parameters
        weights = tf.Variable(tf.truncated_normal(
            shape=[num_pixels_conv, num_pixels_conv, num_channels_in_previous_layer, num_channels_in_current_layer],
            stddev=0.1, name="weights"))
        biases = tf.Variable(tf.constant(0.1, shape=[num_channels_in_current_layer], name="biases"))

        # Activation function
        relu = tf.nn.relu(tf.nn.conv2d(x, weights, strides=[1, strid, strid, 1], padding="SAME") + biases, name=name_scope)

        # TensorBoard Summary
        tf.summary.histogram(relu.op.name + '/activations', relu)

        return relu


def add_max_pool_op(x, name_scope,f,stride):
    return tf.nn.max_pool(x, ksize=[1, f, f, 1], strides=[1, stride, stride, 1], padding="VALID", name=name_scope)



tf_features = tf.placeholder(dtype=tf.float32, shape=[None, image_size, image_size,num_channel],name="tf_features")
labels= tf.placeholder(dtype=tf.int32, shape=[None, NUM_CLASSES],name="tf_targets")
# Input Layer
input_layer = tf.reshape(tf_features, [-1, image_size, image_size, num_channel])


conv_1 = add_conv_op(x=input_layer, num_pixels_conv=7, num_channels_in_previous_layer=3, num_channels_in_current_layer=16, name_scope="conv_1",strid=5)
# print(conv_1.get_shape().as_list(),"conv1")
pool_1 = add_max_pool_op(x=conv_1, name_scope="pool",f=3,stride=3)

# print(pool_1.get_shape().as_list(),"pool_1")

# Second convolution layer
conv_2 = add_conv_op(x=pool_1, num_pixels_conv=2, num_channels_in_previous_layer=16, num_channels_in_current_layer=32, name_scope="conv_2",strid=2)
# print(conv_2.get_shape().as_list(),"conv_2")
pool_2 = add_max_pool_op(x=conv_2, name_scope="pool",f=2,stride=2)

# Reshape the carte-mauritanie from the second convolution for the fully connected layer
shape = pool_2.get_shape().as_list()
pool_reshaped = tf.reshape(pool_2, shape=[-1, shape[1] * shape[2] * shape[3]])
# print(shape,"pool_2")
# Fully connected layer
dense = add_dense_hidden_layer_op(x=pool_reshaped,
             num_neurons_previous_layer=shape[1]*shape[2]*shape[3],
                                  num_neurons_current_layer=120,
                                  name_scope="dense")
shape2 = dense.get_shape().as_list()
# print(shape2)
dense2 = add_dense_hidden_layer_op(x=dense,
             num_neurons_previous_layer=shape2[1],
                                  num_neurons_current_layer=84,
                                  name_scope="dense")
dense3 = add_dense_hidden_layer_op(x=dense2,
             num_neurons_previous_layer=84,
                                  num_neurons_current_layer=32,
                                  name_scope="dense")

# Final softmax layer
softmax,logits,w1 = add_softmax_op(x=dense2, num_neurons_previous_layer=84, num_classes=3, name_scope="softmax")

# predictions = {
#   # Generate predictions (for PREDICT and EVAL mode)
#   "classes": tf.argmax(input=logits, axis=1),
#   # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
#   # `logging_hook`.
#   "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
# }



error = tf.nn.softmax_cross_entropy_with_logits_v2(labels=labels, logits=logits)
regularizer = tf.nn.l2_loss(w1)
loss = tf.reduce_mean(error + beta * regularizer)

# loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

# Configure the Training Op (for TRAIN mode)

optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(
    loss=loss,
    global_step=tf.train.get_global_step())


# Add evaluation metrics (for EVAL mode)
# eval_metric_ops = {
#   "accuracy": tf.metrics.accuracy(
#       labels=labels, predictions=predictions["classes"])}
correct_prediction = tf.equal(tf.argmax(softmax , 1), tf.argmax(labels, 1))

accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32),name="accuracy")
saver = tf.train.Saver()
tf.summary.scalar("cost", loss)
tf.summary.scalar("accuracy", accuracy)
summary_op = tf.summary.merge_all()

num_steps = 100

with tf.Session() as session:
    # variables need to be initialized before we can use them
    session.run(tf.global_variables_initializer())
    # create log writer object
    writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())
    validation_summary_writer = tf.summary.FileWriter(logs_path + '/validation', session.graph)
    # This is a one-time operation which ensures the parameters get initialized as
    # we described in the graph: random weights for the matrix, zeros for the
    # biases.
    images, labels1 = DataLoader("./carte-cdeao/carte-mauritanie/").load_data()
    # images = [cv2.resize(image, (28, 28)) for image in images28]
    print('Initialized')
    trX = images
    (m, n_H0, n_W0, n_C0) = trX.shape

    trY = labels1
    costs = []
    for step in range(num_steps):
        # # Run the computations. We tell .run() that we want to run the optimizer,
        # and get the loss value and the training predictions returned as numpy
        # arrays.
        num_minibatches = int(m / minibatch_size)
        minibatches = random_mini_batches(trX, trY, minibatch_size)

        batch_features = trX
        batch_targets = trY
        # session.run(train, feed_dict={tf_features: batch_features, tf_targets: batch_targets})
        # accur = session.run(accuracy, feed_dict={
        #     tf_features: batch_features,
        #     tf_targets: batch_targets
        # })
        # saver.save(session,"./modelsoftmaxregularise")
        # perform the operations we defined earlier on batch
        # print(correct_prediction)
        for minibatch in minibatches:
            minibatch_cost = 0.
            # Select a minibatch
            (minibatch_X, minibatch_Y) = minibatch

            _, temploss,summary = session.run([train_op, loss, summary_op], feed_dict={tf_features: minibatch_X, labels: minibatch_Y})
            minibatch_cost += temploss / num_minibatches
            saver.save(session, "tmp/modeldeeplearning")
        # if step % 10 == 0:
        #
        #     # Run summaries and measure accuracy on validation set
        #     summary_valid, acc_valid = session.run([summary_op, accuracy],
        #                                         feed_dict={tf_features: valid_dataset, y: valid_labels})
        #
        #     validation_summary_writer.add_summary(summary_valid, step)
        # Print the cost every epoch
        if step % 5 == 0:
            print("Cost after epoch %i: %f" % (step, minibatch_cost))
        if step % 1 == 0:
            costs = [].append(minibatch_cost)

        # write log
        writer.add_summary(summary, num_steps*step)
    # py = session.run(softmax , feed_dict={
    #     tf_features: trX
    # })
    # print("softmax ", py)
    # print("softmax ", np.argmax(py))
    print("Accuracy: ", accuracy.eval(feed_dict={tf_features: trX, labels: trY}))
    print("great!!!")


