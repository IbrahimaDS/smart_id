from PIL import Image
import pytesseract
import argparse
import cv2
import os
import numpy as np
import re
from tensorflow.python.keras._impl.keras.preprocessing.image import load_img, img_to_array

process = "thresh"


def recuperInfos(imageFile, pays):
    inputShape = (224, 224)  # Assumes 3 channel image
    image = load_img(imageFile)
    image = img_to_array(image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.resize(gray, (900, 600))
    h = gray.shape[0]
    w = gray.shape[1]
    for y in range(0, h):
        for x in range(0, w):

            if (pays == "guinee"):
                if ((x < 0.38 * w)):
                    T = 0
                elif ((y > 0.35 * h) & (y < 0.40 * h)):
                    T = 130
                elif ((y < 0.22 * h) | (y > 0.6 * h)):
                    T = 0
                else:
                    T = 125
            elif (pays == "mauritanie"):
                if ((y < 0.24 * h) | (y > 0.86 * h) | ((y > 0.60 * h) & (x > 0.57 * w))):
                    T = 0
                elif ((y > 0.24 * h) & (y < 0.30 * h)):
                    T = 120

                elif ((y > 0.30 * h) & (y < 0.4 * h)):
                    T = 142
                else:
                    T = 130


        gray[y, x] = 255 if gray[y, x] >= T else 0

    if process == "thresh":
        blur = cv2.GaussianBlur(gray, (5, 5), -5)
        blur = blur.astype(np.uint8)
        ret3, th3 = cv2.threshold(blur, 255, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        gray = cv2.adaptiveThreshold(th3, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                     cv2.THRESH_BINARY, 11, 10)

    elif process == "blur":
        gray = cv2.medianBlur(gray, 3)

    filename = "test.jpg".format(os.getpid())
    cv2.imwrite(filename, gray)
    # show the output images
    # cv2.imshow("Image", gray)
    # # cv2.imshow("Output", gray)
    # cv2.waitKey(0)

    # load the image as a PIL/Pillow image, apply OCR, and then delete
    # the temporary file
    text = pytesseract.image_to_string(Image.fromarray(gray))
    # os.remove(filename)
    print(text)
    # if (pays == "mali"):
    #     num = text.splitlines()[0]
    #     nom = text.splitlines()[1]
    #     print(num,nom)
    donnees = []
    l = None
    for line in text.splitlines():
        if (l == 1):
            l = 2
        if (pays == "mali"):
            m1 = re.search(r"((\s*(\d+\s*)[A-Za-z]*)+)", line)
            if (m1 != None):
                donnees.append(m1.group(0))

            m = re.search(r"((\s*[A-Za-z]{2,}(\s*[a-z]*)*)+)", line)
            if ((m != None)):
                donnees.append(m.group(0))
        elif (pays == "guinee"):
            m = re.search(r"(\s*N{1}.{1}\s*(\d+))", line)
            m2 = re.search(r"(Nom (([A-Za-z]\s*)+))", line)
            m3 = re.search(r"(Prénom.? (([A-Za-z]\s*)+))", line)
            m4 = re.search(r"(Sexe.? (([A-Za-z]\s*)+))", line)
            m5 = re.search(r"(N.* (\d{2}/\d{2}/\d{4}))", line)
            if ((m != None)):
                donnees.append(m.group(2))
            if ((m2 != None)):
                donnees.append(m2.group(2))
            if ((m3 != None)):
                donnees.append(m3.group(2))
            if ((m4 != None)):
                donnees.append(m4.group(2))
            if ((m5 != None)):
                donnees.append(m5.group(2))
                l = 1
            if (l == 2):
                donnees.append(line)
                l = 0
        elif (pays == "mauritanie"):
            m = re.search(r"(^\s*(\d+))", line)
            m2 = re.search(r"(^(\s*[A-Z]+(\s*[a-z]*)*)+)", line)
            m3 = re.search(r"(^(\s*\d{2}\s*([A-Za-z]+\s*)+/([A-Za-z]\s*)+\d{4}))", line)
            m4 = re.search(r"^\s*$", line)

            if ((m != None)):
                donnees.append(m.group(2))
                l = 0
            if ((l != None) & (m4 == None)):
                l = l + 1
            if ((l % 2 == 0) & (m2 != None) & (m3 == None)):
                donnees.append(line)
            if (m3 != None):
                donnees.append(m3.group(2))
    # print(donnees)
    return donnees
# recuperInfos("C:/Users/stg_thiam8497/PycharmProjects/reconnaissance/carte-cdeao/carte-mali/7983101.jpg",'mali')

def orderData(donnees,pays):
    infos = None
    if(pays=='mali'):
        infos = {
        'numero': donnees[0],
        'prenom': donnees[1],
        'nom': donnees[2],
        'dateDeNaissance': donnees[3],
        'lieuDeNaissance': donnees[4],
        'NomDeLaMere': donnees[5],
        'NomDuPere': donnees[6],
        'Profession': donnees[6]
        }
    elif(pays=='guinee'):
        infos = {
            'numero': donnees[0],
            'nom': donnees[1],
            'prenom': donnees[2],
            'sexe':donnees[3],
            'dateDeNaissance': donnees[4],
            'lieuDeNaissance': donnees[5],
            'NomDuPere': donnees[6],
            'NomDeLaMere': donnees[7],
            'Profession': donnees[8]
        }
    else:
        infos = {
            'numero': donnees[0],
            'prenom': donnees[1],
            'NomDuPere': donnees[2],
            'nom': donnees[4],
            'sexe':donnees[5],
            'dateDeNaissance': donnees[6],
            'lieuDeNaissance': donnees[7],
        }

    return infos

