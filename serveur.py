import numpy as np
from flask import Flask
from flask import request
from flask import jsonify
from PIL import Image


from tensorflow.python.keras import Model
from tensorflow.python.keras._impl.keras.applications import VGG16
from tensorflow.python.keras._impl.keras.layers import Dense, Dropout, Flatten
from tensorflow.python.keras._impl.keras.preprocessing.image import load_img, img_to_array

from textocr import performOcr,orderData


HOST = 'localhost:9000'
MODEL_NAME = 'test'
MODEL_VERSION = 1


app = Flask(__name__)

img_size=224
base_model = VGG16(include_top=False, weights=None, input_shape = (img_size, img_size, 3))
x = base_model.output
x = Flatten()(x)
x = Dense(1024, activation="relu")(x)
x = Dropout(0.5)(x)
classes = 5
imageFile= "C:\\Users\\ibrahima\\Downloads\\ibrahima_ci.jpeg"
predictions = Dense(classes, activation='softmax')(x)
model = Model(inputs=base_model.input, outputs=predictions)

model.load_weights("cv-tricks_fine_tuned_model4.h5")

classes_predict=['autres','guinee','mali','mauritanie','senegal']

@app.route('/prediction',methods=['POST'])
def predict():


    imageFile = request.files['image']
    inputShape = (224, 224)  # Assumes 3 channel image
    image = load_img(imageFile, target_size=inputShape)
    image = img_to_array(image)  # shape is (224,224,3)
    image = np.expand_dims(image, axis=0)  # Now shape is (1,224,224,3)
    image = image / 255.0
    preds = model.predict(image)
    best_classe_indice = np.argmax(preds,axis=1)
    best_classe_proba = preds[np.arange(len(best_classe_indice)),best_classe_indice]
    print(best_classe_proba[0])
    if((classes_predict[best_classe_indice[0]]!='autres') & (best_classe_proba[0] > 0.85)) :
        donnees = performOcr(imageFile,classes_predict[best_classe_indice[0]])
        infos=orderData(donnees,classes_predict[best_classe_indice[0]])
        prediction = { 'classe':str(classes_predict[best_classe_indice[0]]),'probabilite':float(best_classe_proba[0]),'informations':infos}
    else:
        prediction = {'classe': str(classes_predict[best_classe_indice[0]]),
                      'probabilite': float(best_classe_proba[0]),'informations':None}

    return jsonify(prediction)
if __name__ == '__main__':
    app.run(host='0.0.0.0',port=3000)
