import time

from EvaluationOp import EvaluationOp
from InferenceOp import InferenceOp
from dataloader import DataLoader
import tensorflow as tf

import numpy as np

from loss_op import LossOp
from training_op import TrainingOp

NUM_PIXELS = 300*500
NUM_CLASSES = 3
learning_rate = 0.001
batch_size = 10
max_steps = 100
summaries_dir = "/tmp/not_mnist/not_mnist_logs"




def main():
    """
    Train NotMNIST for a number of steps.
    """
    # Load data
    train_dataset, train_labels = DataLoader("./carte-cdeao/").load_data()
    with tf.Graph().as_default():
        global_step = tf.Variable(0, name='global_step', trainable=False)

        # Start running operations on the Graph.
        sess = tf.Session()

        """
        Step 1 - Input data management
        """

        x = tf.placeholder(dtype=tf.float32, shape=[None, 300, 500])
        y = tf.placeholder(dtype=tf.int32, shape=[None, NUM_CLASSES])

        # Reshape images for visualization
        x_reshaped = tf.reshape(x, [-1, NUM_PIXELS])
        tf.summary.image('input', x_reshaped, NUM_CLASSES)

        """
        Step 2 - Building the graph
        """

        # Build a Graph that computes the logits predictions from the inference model.
        softmax, logits = InferenceOp(num_pixels=NUM_PIXELS, num_classes=NUM_CLASSES).add_ops(x)

        print("ici!")

        # # Calculate loss.
        loss = LossOp(name_scope="cross_entropy").add_op(logits, y)

        # Build a Graph that trains the model with one batch of examples and updates the model parameters.
        train_op = TrainingOp(learning_rate=learning_rate, name_scope="train").add_op(loss, global_step)

        """
        Step 3 - Build the evaluation step
        """

        # Model Evaluation
        accuracy = EvaluationOp(name_scope="accuracy").add_op(softmax, y)

        """
        Step 4 - Merge all summaries for TensorBoard generation
        """
        # Create a saver.
        saver = tf.train.Saver(tf.global_variables())

        # Build the summary operation based on the TF collection of Summaries.
        summary_op = tf.summary.merge_all()

        # Summary Writers
        train_summary_writer = tf.summary.FileWriter(summaries_dir + '/train', sess.graph)
        validation_summary_writer = tf.summary.FileWriter(summaries_dir + '/validation', sess.graph)

        """
        Step 5 - Train the model, and write summaries
        """

        # Build an initialization operation to run below.
        init = tf.global_variables_initializer()
        sess.run(init)

        for step in range(max_steps):

            start_time = time.time()

            # Pick an offset within the training data, which has been randomized.
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)

            # Generate a minibatch.
            batch_data = train_dataset[offset:(offset + batch_size), :]
            batch_labels = train_labels[offset:(offset + batch_size), :]

            # Run training step and train summaries
            summary_train, _, loss_value = sess.run([summary_op, train_op, loss],
                                                    feed_dict={x: batch_data, y: batch_labels})

            train_summary_writer.add_summary(summary_train, step)

            duration = time.time() - start_time

            if step % 10 == 0:
                num_examples_per_step = batch_size
                examples_per_sec = num_examples_per_step / duration
                sec_per_batch = float(duration)

                # # Run summaries and measure accuracy on validation set
                # summary_valid, acc_valid = sess.run([summary_op, accuracy],
                #                                     feed_dict={x: valid_dataset, y: valid_labels})
                #
                # validation_summary_writer.add_summary(summary_valid, step)

                # format_str = '%s: step %d, accuracy = %.2f (%.1f examples/sec; %.3f sec/batch)'
                # print (format_str % (datetime.now(), step, 100 * acc_valid, examples_per_sec, sec_per_batch))
        #
        # acc_test = sess.run(accuracy, feed_dict={x: test_dataset, y: test_labels})
        # print ('Accuracy on test set: %.2f' % (100 * acc_test))