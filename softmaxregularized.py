# This is to expedite the process
import random

train_subset = 10000
# This is a good beta value to start with
import tensorflow as tf

import numpy as np
import matplotlib.pyplot as plt

from dataloader import DataLoader

beta = 0.001
image_size = 300
num_labels = 3

NUM_PIXELS = image_size*image_size*3
NUM_CLASSES = 3
learning_rate = 0.001


graph = tf.Graph()
with graph.as_default():
    # Input data.
    # They're all constants.
    images ,labels  = DataLoader("./carte-cdeao/carte-mauritanie/").load_data()
    # tf_valid_dataset = tf.constant(valid_dataset)
    # tf_test_dataset = tf.constant(test_dataset)

    # Variables
    # They are variables we want to update and optimize.
    tf_features = tf.placeholder(dtype=tf.float32, shape=[None, image_size, image_size,3],name="tf_features")
    tf_targets = tf.placeholder(dtype=tf.int32, shape=[None, NUM_CLASSES],name="tf_targets")
    w1 = tf.Variable(tf.random_normal([NUM_PIXELS, NUM_CLASSES]))
    b1 = tf.Variable(tf.zeros([NUM_CLASSES]))

    # Training computation.
    logits = tf.matmul(tf.reshape(tf_features, [-1, NUM_PIXELS]), w1) + b1
    # Original loss function
    error = tf.nn.softmax_cross_entropy_with_logits_v2(labels=tf_targets, logits=logits)
    loss = tf.reduce_mean(error)
    # Loss function using L2 Regularization
    regularizer = tf.nn.l2_loss(w1)
    loss = tf.reduce_mean(error + beta * regularizer)

    # Optimizer.
    train = tf.train.GradientDescentOptimizer(0.5).minimize(loss)

    # Predictions for the training, validation, and test data.
    softmax = tf.nn.softmax(logits,name="softmax")
    correct_prediction = tf.equal(tf.argmax(softmax, 1), tf.argmax(tf_targets, 1))

    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32),name="accuracy")
    saver = tf.train.Saver()
    # valid_prediction = tf.nn.softmax(tf.matmul(tf_valid_dataset, weights) + biases)
    # test_prediction = tf.nn.softmax(tf.matmul(tf_test_dataset, weights) + biases)

num_steps = 100





with tf.Session(graph=graph) as session:
    # This is a one-time operation which ensures the parameters get initialized as
    # we described in the graph: random weights for the matrix, zeros for the
    # biases.
    session.run(tf.global_variables_initializer())
    print('Initialized')
    trX = np.array(images)

    trY = np.array(labels)
    py = session.run(softmax, feed_dict={
        tf_features: trX
    })
    print("softmax ", py)
    prediction = tf.argmax(softmax, 1)
    print(prediction.eval(feed_dict={tf_features: images}))
    for step in range(num_steps):
        # # Run the computations. We tell .run() that we want to run the optimizer,
        # and get the loss value and the training predictions returned as numpy
        # arrays.

        batch_features = trX
        batch_targets = trY
        session.run(train, feed_dict={tf_features: batch_features, tf_targets: batch_targets})
        accur = session.run(accuracy, feed_dict={
            tf_features: batch_features,
            tf_targets: batch_targets
        })
        saver.save(session,"./modelsoftmaxregularise")


    # print("softmax ", np.argmax(py))
    print(accur)
